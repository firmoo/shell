#!/bin/bash
# elasticsearch clean job
# 删除早于{n}天的ES集群的索引
# 支持日期格式 index-YYYY/MM/DD

# input args
day="$1"
host="$2"
scheme="$3"
port="$4"
user="$5"
password="$6"
reqUrl=""
# input args end

# 定义删除索引函数
function delete_indices() {
    comp_date=`date -d "$day day ago" +"%Y-%m-%d"`
    date1="$1"
    date2="$comp_date"

    t1=`date -d "$date1" +%s` 
    t2=`date -d "$date2" +%s` 

    if [ $t1 -le $t2 ]; then
        echo "$1<=$comp_date，进行索引删除"
        #转换一下格式，将类似2022-08-01格式转化为2022.08.01
        format_date=`echo $1| sed 's/-/\./g'`
        curl -s  -XGET "$reqUrl/_cat/indices?v"|grep $format_date | awk -F '[ ]+' '{print $3}' 
        curl -s -XDELETE $reqUrl/*$format_date
    fi
}
# 生成URL
if [ -n "$user" ] || [ -n "$password" ]; then
   reqUrl="$scheme://$user:$password@$host:$port"
   else
   reqUrl="$scheme://$host:$port"
fi
# 获取索引名称日期
curl -s  -XGET "$reqUrl/_cat/indices?v" | awk -F" " '{print $3}' | awk -F"-" '{print $NF}' | egrep "[0-9]*\.[0-9]*\.[0-9]*" | sort | uniq  | sed 's/\./-/g' | while read LINE
do
    #调用索引删除函数
    delete_indices $LINE
done 