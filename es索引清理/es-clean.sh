#!/bin/sh
# elasticsearch clean job
# 每天检测滚动索引日期在{n}天前的删除
# 支持日期格式 index-YYYY/MM/DD
# @date 2022/5/27 15:00
# version v1.0

# input args
day="$1"
host="$2"
scheme="$3"
port="$4"
user="$5"
password="$6"
reqUrl=""
# input args end

# 获取30天前的日期
DATE=$(date -d "$day days ago" +%Y.%m.%d)
#获取对应日期索引
if [ -n "$user" ] || [ -n "$password" ]; then
   reqUrl="$scheme://$user:$password@$host:$port"
   else
   reqUrl="$scheme://$host:$port"
fi
#清理索引
curl -s  -XGET "$reqUrl/_cat/indices?v"| grep "$DATE" | awk -F '[ ]+' '{print $3}' >/tmp/elk.log
if [ -s /tmp/elk.log ]; then
   echo "[info] GET es index ok."
   else
     echo "[error] GET index fail.Please check connect."
     exit 0
fi
while IFS= read -r i;
do
  curl  -XDELETE  "$reqUrl/$i"
  echo "清理过期索引：$i";
done < /tmp/elk.log




